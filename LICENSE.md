© Головин Г.Г., 2021-2023

Опубликовано под [Открытой лицензией 1.1](OPEN_LICENSE.txt)

Рекомендуется не удалять и не изменять упоминания об Авторе, содержащиеся в Программе. Это
правило также применяется к любым значимым изменениям Программы, в которых информацию об
Авторе и о создании Программы рекомендуется сохранять.

---

© Golovin G.G., translation from Russian, 2021-2023

Published under the [Open License 1.1](OPEN_LICENSE.txt)

It is recommended not to delete and not to change the references to the Author contained in
the Program. This rule also applies to any significant changes of the Program, in which the
information about the Author and the creation of the Program is recommended to be saved.
