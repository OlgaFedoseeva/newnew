# сборка сайта в двух помидорных темах и оптимизация результатов
rm -rf _site
rm -rf _site_older
rm -rf _site_color
# сборка старого помидора
mkdir -p _site_older
cp -r jekyll_site/_includes _site_older
cp -r jekyll_site/ru _site_older
cp -r jekyll_site/en _site_older
cp -r jekyll_site/ru/index.md _site_older
cp -r jekyll_site/_config_older.yml _site_older/_config.yml
cp -r jekyll_site/Gemfile_older _site_older/Gemfile
cd _site_older || exit
jekyll build
cp -r _site ..
cd ..
# сборка цветного помидора
mkdir -p _site_color
cp -r jekyll_site/_includes _site_color
cp -r jekyll_site/ru _site_color
cp -r jekyll_site/en _site_color
cp -r jekyll_site/ru/index.md _site_color
cp -r jekyll_site/_config_color.yml _site_color/_config.yml
cp -r jekyll_site/Gemfile_color _site_color/Gemfile
cd _site_color || exit
jekyll build
cp -r _site ../_site/color
cd ..
# копирование без сборки
cp -r jekyll_site/img _site
cp -r jekyll_site/robots.txt _site
# оптимизация собранного контента
cd _site || exit
cp -r assets/* .
rm -r assets
rm -r color/assets/favicon.ico
cp -r color/assets/* .
rm -r color/assets
rm -r color/404.html
find . -type f -name '*.html' -exec sed -i -r 's/layout-padding=""/layout-padding/g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/ class="language-plaintext highlighter-rouge"//g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/ class="language-java highlighter-rouge"//g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/<div><div class="highlight"><pre class="highlight">/<div class="highlight"><pre class="highlight">/g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/<\/code><\/pre><\/div><\/div>/<\/code><\/pre><\/div>/g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/<hr \/>/<hr>/g' {} \;
find . -type f -name '*.html' -exec sed -i -r 's/<img(.+) \/>/<img\1>/g' {} \;
# отчет о завершении
echo "Build completed on: $(date)"
